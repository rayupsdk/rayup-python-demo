#!/usr/bin/python3
# -*- coding:utf-8 -*-

import requests
import json
import hashlib
import base64
import hmac
import random
from datetime import datetime
from urllib.parse import urlencode
from urllib.parse import quote_plus


RUS_SIG_METHOD = "HMAC-SHA1"
RUS_SIG_VERSION = "1"
RUS_CONTENT_TYPE = "application/json;charset=utf-8"

RUS_ACCESS_KEY_ID = "727965f2c89511e8af75560001a43649"
RUS_ACCESS_KEY_SECRET = "cnlo/siVEeivdVYAAaQ2SQ=="

RUS_API_URL_BASE = "https://api.rayup.io"


def make_rusapi_headers(method, path, query=None, data=None):

    m = hashlib.md5()
    if data is None: data = b""
    m.update(data)
    content_md5 = base64.b64encode(m.digest()).decode()

    GMT_FORMAT = '%a, %d %b %Y %H:%M:%S GMT'
    now_gmt = datetime.utcnow().strftime(GMT_FORMAT)

    sig_nonce = str(random.randint(2**31, 2**32))

    canonicalized_headers = 'x-rus-signature-method:' + RUS_SIG_METHOD + '\n' \
            + 'x-rus-signature-nonce:' + sig_nonce + '\n' \
            + 'x-rus-signature-version:' + RUS_SIG_VERSION

    # canonicalized resource
    canonicalized_resource = path
    if query is not None:
        conditions = ''
        field_names = []
        for key in query.keys():
            field_names.append(key)
        if len(field_names) > 0:
            field_names.sort()
            conditions = ''
            for key in field_names:
                if len(conditions) > 0:
                    conditions += '&'
                conditions += '{}={}'.format(key, quote_plus(str(query.get(key))))
            canonicalized_resource += '?' + conditions

    to_sign = method + '\n' \
            + content_md5 + '\n' \
            + now_gmt + '\n' \
            + canonicalized_headers + '\n' \
            + canonicalized_resource
    print("==> to sign: " + to_sign)

    # authorization
    dig = hmac.new(RUS_ACCESS_KEY_SECRET.encode(encoding='utf-8'),
                   msg=to_sign.encode(encoding='utf-8'),
                   digestmod=hashlib.sha1).digest()
    cor_sig = base64.b64encode(dig).decode()

    headers = {
        'Date': now_gmt,
        'Content-Type': RUS_CONTENT_TYPE,
        'Content-MD5': content_md5,
        'x-rus-signature-method': RUS_SIG_METHOD,
        'x-rus-signature-version': RUS_SIG_VERSION,
        'x-rus-signature-nonce': sig_nonce,
        'Authorization': 'rus {}:{}'.format(RUS_ACCESS_KEY_ID, cor_sig)
    }
    print("==> request:")
    print("{} {}?{}".format(method, path, [query if query is not None else '']))
    print("{}".format(json.dumps(headers, indent=2)))
    return headers


def print_rusapi_resp(r):
    print("==> received response:")
    if r.status_code == 200:
        print(json.dumps(r.json(), indent=2))
    else:
        print(r)


def rusapi_test(uri, req_param, title=""):
    print("\n===========> TEST {} ==============\n".format(title))
    url = RUS_API_URL_BASE + uri + "?" + urlencode(req_param)
    headers = make_rusapi_headers("GET", uri, req_param)
    r = requests.get(url, headers=headers)
    print_rusapi_resp(r)


def main():
    rusapi_test("/v1/coin/map", {"page":1, "count":12, "status":"active"}, "get_coin_map")
    rusapi_test("/v1/coin/info", {"symbol": "ETH,BRM,BTC,BELA"}, "get_coin_info")

    rusapi_test("/v1/coin/quotes/latest", {"code":2, "convert":"CNY"}, "get_coin_market_pairs_latest case1")
    rusapi_test("/v1/coin/quotes/latest", {"code":2, "convert":"CNY,EUR"}, "get_coin_market_pairs_latest case2")
    rusapi_test("/v1/coin/quotes/latest", {"code":540, "convert":"CNY"}, "get_coin_market_pairs_latest case3")
    rusapi_test("/v2/coin/quotes/latest", {"code":"2657,2", "convert":"CNY,EUR"}, "get_coin_market_pairs_latest case4")
    rusapi_test("/v3/coin/quotes/latest", {"code":"2657,2", "convert":"CNY,EUR"}, "get_coin_market_pairs_latest case5")
    rusapi_test("/v1/coin/pairs/latest", {"left":2657, "right":"1027,825"}, "get_coin_pairs_latest")

    rusapi_test("/v1/currency/rates/latest", {"left":"USD", "right":"USD"}, "get_currency_rates_latest")
    rusapi_test("/v1/currency/rates/latest", {"left":"USD", "right":"CNY"}, "get_currency_rates_latest")
    rusapi_test("/v1/currency/rates/latest", {"left":"GBP", "right":"CNY"}, "get_currency_rates_latest")
    
    rusapi_test("/v1/eth/txs/0x9a24eeb7dfde9575bbe7fceeb9ebe5897b00a2293519abe01f22b2a053d24b7d", {}, "get_tx_by_hash")
    rusapi_test("/v1/eth/blocks/0xd7a94f8e98c38dae2a4e96f8f55de01ee221ab86ce611dff52f2da02cf3c5ec8", {}, "get_block_by_hash")
    rusapi_test("/v1/eth/blocks/6824754", {}, "get_block_by_height")

    rusapi_test("/v1/eth/tokens", {"page":1, "count": 10}, "load_eth_tokens")
    rusapi_test("/v1/eth/tokens/2e98a6804e4b6c832ed0ca876a943abd3400b224", {}, "get_token_by_address case1")
    rusapi_test("/v1/eth/tokens/0x2e98a6804e4b6c832ed0ca876a943abd3400b224", {}, "get_token_by_address case2")


if __name__ == '__main__':
  main()

