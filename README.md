# RayUp Python Demo

RayUp Service (RUS) api demo with python language.

## Enviroment and dependencies

- Python 3+

- Requests

```shell
$ pip3 install requests
```

## Run

```shell
$ python3 rusapi-demo.py
```
